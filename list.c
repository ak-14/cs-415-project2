#include <stdlib.h>
#include <stdio.h>
#include "list.h"



void remove_node (node_t **list, node_t *node, void(*func)(void *))
{   
    int size;

    if (*list == NULL) return;

    size = (*list)->size;

    if (node == NULL) return;

    if (node->prev == NULL && node->next != NULL)
    {
        node->next->prev = NULL;
    }
    else if ( node->next == NULL && node->prev != NULL)
    {
        node->prev->next = NULL;
    }
    else if ( node->next != NULL && node->prev != NULL)
    {
        node->prev->next = node->next;
        node->next->prev = node->prev;
    }

    size--;

    func(node->data);
    free (node);

    if (size == 0) {
        *list = NULL;
        return;
    }

    (*list)->size = size;
}


node_t *list_find (node_t *node, int(*func)(void*,pid_t), pid_t id)
{
    while(node)
    {
        if( func(node->data, id) > 0 ) 
            return node;

        node = node->next;
    }
    return NULL;
}




node_t *push(node_t *list, void *data)
{   
    if ( data == NULL ) return NULL;

    node_t *node;

    node = malloc( sizeof(node_t) );

    node->data = data;

    node->prev = NULL;
    node->next = NULL;

    if ( list == NULL )
    {   
        list = node;
        list->size = 0;
    }
    else
    {
        node->size = list->size;
        list->prev = node;
        node->next = list;
        list = node;

    }
    list->size++;
    return list;
}


void destroy_list(node_t **list, void(*func)(void*))
{

    while(*list != NULL)
    {
        node_t *temp = *list;
        if ((*list)->next == NULL) 
        {
            func( (*list)->data );
            free( (*list) );
            *list = NULL;
            break;
        }
        *list = (*list)->next;
        func(temp->data);
        free(temp);
    }
}
