#ifndef __PROCESS_H__
#define __PROCESS_H__

#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>

/**
 * A process structure that stores the process id and the group id.
 */
typedef struct process_s {
    pid_t pid;
    pid_t pgrp;
} process_t;


/**
 * Initialize a process and return a pointer to it.
 */
process_t *init_process (pid_t pid, pid_t pgrp);


/**
 * Compare a process struct with a pid.
 */
int process_cmp (void *pro, pid_t pid);


/**
 * Free the allocated space.
 */   
void free_process ( void *pro );


#endif
