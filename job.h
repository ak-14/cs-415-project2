#ifndef __JOB_H__
#define __JOB_H__

#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>


/**
 * A job structure that stores the group id and the job name.
 */
typedef struct job_s {
    pid_t pgrp;
    char *job_name;
} job_t;


/**
 * Initialize a job and return a pointer to it.
 */
job_t *init_job (pid_t pgrp, char *job_str, int size);


/**
 * Compare a jobe struct with a pgrp.
 */
int job_cmp (void *job, pid_t pgrp);


/**
 * Free the allocated space.
 */   
void free_job ( void *job );


#endif
