Abdulrahman Alkhelaifi
CIS 415 - Project 2
May 31, 2013


*GRACE DAYS: 2


List of files:

 - kind-sh.c
 - utils.c
 - utils.h
 - command.c
 - command.h
 - process.c
 - process.h
 - job.c
 - job.h
 - list.c
 - list.h
 - tokenizer.c
 - tokenizer.h


Extra credit:


Overview:

 - implementation of a fully funcitonal command line shell using a sychronous
   approach.
 - modified tokenizer to detect 2> which is stderr redirection symbol as a single
   token to ditingush between ( 2 > ) where 2 is an argument and ( 2> ).
 - testing the shell under valgrind to make sure there are no memory leaks.
 - tried to abstract, structure, and organize the code as much as possible.

 

Description of code:

 - a main file "kind-sh" that includes the main funtion
 - most of the utilites are in the "utils" file
 - a command struct that represents commands between pipes
 - an abstract list type used for a list of process and a list of jobs

General comments:

 - If time allowed, I would have done many things including:
   - asynchronous job control
   - extensive testing
   - add the extra functionality of the extra credit problems
   - add more stuff for fun such as history of commands and << >>


Number of hours spent:

 - lost count long time ago! but definitely more than 5 days ~15h/day

