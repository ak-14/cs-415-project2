#include <stdio.h>
#include <stdlib.h>
#include "process.h"
#include "list.h"
#include "utils.h"


/**
 * Initialize a process and return a pointer to it.
 */
process_t *init_process (pid_t pid, pid_t pgrp)
{
    process_t *pro;

    pro = malloc(sizeof(process_t));

    pro->pid = pid;
    pro->pgrp = pgrp;

    return pro;
}


/**
 * Compare a process struct with a pid.
 */
int process_cmp (void *pro, pid_t pid)
{
    if ( ((process_t*) pro)->pid == pid )
        return 1;

    return 0;
}


/**
 * Free the allocated space.
 */   
void free_process ( void *pro )
{
    if ( pro != NULL )
    {
        free(pro);
    }
}
