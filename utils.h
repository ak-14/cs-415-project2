#ifndef __UTILS_H__
#define __UTILS_H__

#include <unistd.h>
#include <sys/types.h>
#include "command.h"

#define true 1
#define false 0

enum shell_cmd_t { EXIT, FG, BG, JOBS, COMMAND };


/**
 *  Compare tow strings.
 */
int my_strcmp( char *s1, char *s2 );


/**
 *  Return the legth of a string.
 */
int my_strlen( char *s );


/**
 *  Copy one buffer to another up to size
 */
char * my_strncpy(char *dest, const char *src, int size);


/**
 *  Return the type of user's input.
 */
enum shell_cmd_t shell_cmds( char * buffer );


/**
 *  Parse user input into an array of commands.
 */
int parse(char *buffer, command ***cmds_arr);


/** 
 * Execute a command.
 */
void exec_cmd (command * cmd, int fds[][2]);


/**
 *  Execute fg command
 */
int foregound (pid_t shell_pgrp, pid_t pgrp, int * status);

#endif
