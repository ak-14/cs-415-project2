CFLAGS=-g -Wall
CC=gcc
SRCS=tokenizer.c command.c utils.c process.c job.c list.c
OBJS=tokenizer.o command.o utils.o process.o job.o list.o
LDFLAGS= -g
LIBS=

all:      kinda-sh

kinda-sh: $(OBJS)
	$(CC) $(LDFLAGS) $(LIBS) $(OBJS) kinda-sh.c -o kinda-sh

$(OBJS):  $(SRCS)
	$(CC) $(CFLAGS) -c $*.c

clean:
	rm -f *.o kinda-sh

test:
	valgrind --tool=memcheck --leak-check=full --show-reachable=yes --num-callers=20 --track-fds=yes ./kinda-sh
