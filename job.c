#include <stdio.h>
#include <stdlib.h>
#include "job.h"
#include "list.h"
#include "utils.h"


/**
 * Initialize a job and return a pointer to it.
 */
job_t *init_job (pid_t pgrp, char *job_name, int size)
{
    job_t *loc_job;
    char *str;

    loc_job = malloc(sizeof(job_t));
    str = malloc(size);

    loc_job->pgrp = pgrp;

    my_strncpy (str, job_name, size);

    loc_job->job_name = str;

    return loc_job;
}


/**
 * Compare a jobe struct with a pgrp.
 */
int job_cmp (void *job, pid_t pgrp)
{
    if ( ((job_t *)job)->pgrp == pgrp )
        return 1;

    return 0;
}


/**
 * Free the allocated space.
 */      
void free_job ( void *job )
{
    if (job == NULL) return;

    free( ((job_t *)job)->job_name );
    free(job);
}
