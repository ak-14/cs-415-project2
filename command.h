#ifndef __COMMAND_H__
#define __COMMAND_H__


enum pipe_t { NONE, IN, OUT, BOTH };


/**
 * Control structure for a command.
 */
typedef struct command {
    char **args;
    int size, bkg;
    char *file_in, *file_out, *file_err;
    enum pipe_t pipe;
} command;


/**
 * Build a command from a set of arguments.
 */
command *build_command( char **args, int size, int bkg, char *file_in, char *file_out, char *file_err, enum pipe_t pipe );


/**
 * Deallocates space used by the command.
 */
void free_command( command *cmd );


/**
 * Deallocates space used by the array of commands.
 */
void free_cmd_arr( command **cmds_arr, int num_cmds );



#endif
