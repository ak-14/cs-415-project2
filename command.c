#include <stdio.h>
#include <stdlib.h>
#include "command.h"



/**
 * Build a command from a set of arguments.
 */
command *build_command( char **args, int size, int bkg, char *file_in, char *file_out, char *file_err, enum pipe_t pipe ) 
{
    command *cmd;

    cmd = malloc( sizeof(command) );

    cmd->args = args;
    cmd->size = size;
    cmd->bkg = bkg;

    cmd->file_in = file_in;     
    cmd->file_out = file_out;
    cmd->file_err = file_err;
    cmd->pipe = pipe;

    return cmd;
}


/**
 * Deallocates space used by the command.
 */
void free_command ( command *cmd ) {
    int i;

    if ( cmd == NULL ) return;

    if ( cmd->args != NULL ) {
        for ( i = 0; i < cmd->size; i++ ) 
        {
            free( cmd->args[i] );
        }
        free(cmd->args);
        cmd->args = NULL;
    }

    if ( cmd->file_in != NULL ) 
    {
        free( cmd->file_in );
        cmd->file_in = NULL;
    }

    if ( cmd->file_out != NULL ) 
    {
        free( cmd->file_out );
        cmd->file_out = NULL;
    }

    if ( cmd->file_err != NULL ) 
    {
        free( cmd->file_err );
        cmd->file_err = NULL;
    }

    free( cmd );
    cmd = NULL;
}


/**
 * Deallocates space used by the array of commands.
 */
void free_cmd_arr ( command **cmds_arr, int num_cmds )
{
    int i;

    if ( cmds_arr == NULL ) return;

    for ( i = 0; i < num_cmds ; i++ )
        free_command( cmds_arr[i] );

    free( cmds_arr );
    cmds_arr = NULL;
}
