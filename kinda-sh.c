#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "tokenizer.h"
#include "command.h"
#include "utils.h"
#include "process.h"
#include "job.h"
#include "list.h"


/**
 * Main program execution
 */
int main( int argc, char *argv[] ) 
{
    char buffer[1024], *copy;
    int br, status, num_cmds, cmds_i = 0;
    pid_t pid, pid2, pgrp, shell_pgrp;
    int fds[0][2]; /* either put arbitrary or malloc later */
    int cmd_type;
    command **cmds_arr;
    command *cmd;
    process_t *pro;
    job_t *job;
    node_t *job_list = NULL, *pro_list = NULL, *node = NULL;

    //TO REMOVE HEAD head = head->prev remove head->next head next null

    buffer[1023] = '\0';


    /* make shell group leader */
    if ( setpgid (getpid(), getpid()) == -1 )
    {
        perror(__func__);
        exit(EXIT_FAILURE);
    }


    /* store process group of the shell */
    shell_pgrp = getpid();


    /* give the shell terminal control */
    if( tcsetpgrp (STDIN_FILENO, getpgid(0)) == -1 )
    {
        perror(__func__);
        exit(EXIT_FAILURE);
    }

    /* igonre signals for a synchronous approach */
    signal (SIGTTIN, SIG_IGN);
    signal (SIGTTOU, SIG_IGN);
    signal (SIGQUIT, SIG_IGN);
    signal (SIGTSTP, SIG_IGN);
    signal (SIGINT, SIG_IGN);
    //signal (SIGCHLD, SIG_IGN);


    while (1) 
    {

        report_jobs (pro_list, job_list );


        if ( (write(STDOUT_FILENO, "kind-sh> ", 9)) == -1 )
        {
            perror(__func__);
            exit(EXIT_FAILURE);
        }


        if ( (br = read(STDIN_FILENO, buffer, 1024)) == -1 )
        {
            perror(__func__);
            exit(EXIT_FAILURE);
        }

        buffer[br-1] = '\0';


        /* save buffer for process */
        //SHOULD USE TOKENIZER to get rid of spaces
        copy = malloc(br);
        my_strncpy(copy, buffer, br);


        /* user enters nothing */
        if ( br == 1 ) continue; 


        /* handle exit, bg, fg, and jobs */
        cmd_type = shell_cmds(buffer);

        switch ( cmd_type ) 
        {
            case EXIT:
                destroy_list(&job_list, free_job);
                destroy_list(&pro_list, free_process);
                free(copy);
                exit(EXIT_SUCCESS);
            case FG:
                // node = list_find (job_list, job_cmp, pgrp);
                // job = node->data;
                printf("Running: %d\n", pgrp);
                foregound(shell_pgrp, pgrp, &status);
                break;
            case BG:
                printf("Running: %d\n", pgrp);
                killpg(pgrp, SIGCONT);
                break;
            case JOBS:
                printf("This command is not supported in this version!\n");
                break;
            case COMMAND:
                break;
        }


        /* no need to continue if no commands are entered */
        if ( cmd_type != COMMAND ) continue;


        /* need to be reset */
        cmds_i = num_cmds = 0; 


        /* parse user input */
        if ( (num_cmds = parse(buffer, &cmds_arr)) == -1 )
        {
            printf("Command can't be parsed. Check for overlapping redirections and pipes.\n");
            continue;
        }


        /* for now only two commands max */
        if (num_cmds > 2)
        {
            printf("Shell only handles two stage pipeline in this version!\n");
            free_cmd_arr(cmds_arr, num_cmds);
            continue;
        }


        /* get the first command */
        cmd = cmds_arr[cmds_i++];


        /* create a pipe or multiple pipes */
        if ( pipe(fds[0]) == -1 )
        {
            perror(__func__);
            exit(EXIT_FAILURE);
        }


        /* fork the first child */
        if ( (pid = fork()) == -1 )
        {
            perror(__func__);
            exit(EXIT_FAILURE);
        }


        /* child process */
        if (pid == 0)  
        {
            /* set signals back to default for child */
            signal (SIGQUIT, SIG_DFL);
            signal (SIGTSTP, SIG_DFL);
            signal (SIGTTIN, SIG_DFL);
            signal (SIGTTOU, SIG_DFL);
            signal (SIGCHLD, SIG_DFL);
            signal (SIGINT, SIG_DFL);


            /* Make yourself process group leader */
            if( setpgid(0,0) == -1 ) 
            {
                perror(__func__);
                exit(EXIT_FAILURE);
            }


            /* give terminal control to child if running in foreground */

            // cmd-bkg might cause problems better check for jobs
            // well .. there has to be one command at least with bkg true
            if( cmd->bkg == false && num_cmds == 1 )
            { 
                if( tcsetpgrp (STDIN_FILENO, getpgid(0)) == -1 )
                {
                    perror(__func__);
                    exit(1);
                }
            }

            exec_cmd(cmd, fds); //handle error
        }


        /* parent process (shell) */
        else
        { 

            /* set pgrp to child's pid */
            setpgid(pid, pid);
            pgrp = pid;


            /* execute here and in child to avoid racing */
            if (cmd->bkg == false && num_cmds == 1)
            {
                if ( tcsetpgrp (STDIN_FILENO, getpgid(pid)) == -1 )
                {
                    perror(__func__);
                    exit(EXIT_FAILURE);
                }
            }

            /* create process */
            pro = init_process(pid, pgrp);

            /* add to list of processes */
            pro_list = push(pro_list, pro);


            /*create job */
            job = init_job(pgrp, copy, br);

            /* add to list of jobs */
            job_list = push(job_list, job);



        }


        /* block of code for multiple commands only */
        if ( num_cmds > 1 ) 
        {

            /* get second command */
            cmd = cmds_arr[cmds_i++];


            /* fork second child */
            if ( (pid = fork()) == -1 )
            {
                perror(__func__);
                exit(EXIT_FAILURE);
            }


            /* second child */
            if (pid == 0) 
            {

                /* set group to first child's group */
                setpgid(0, pgrp); 


                /* Not sure if needed. Probably safer since it has no affect if repeated */

                /* transfer controlling terminal. needed because bkg can be in any child*/
                if ( cmd->bkg == false ) {
                    //can't use fucking stdin because it's redirected!!!
                    if( tcsetpgrp (STDOUT_FILENO, getpgid(0)) == -1 ) //getpgid
                    {
                        perror(__func__);
                        exit(EXIT_FAILURE);
                    }
                }

                exec_cmd(cmd, fds); //handle error
            }

            /* create process */
            pro = init_process(pid, pgrp);

            /* add to list of processes */
            pro_list = push(pro_list, pro);
        } 

        /* still in parent */
        /* if multiple pipes loop */
        close(fds[0][0]);
        close(fds[0][1]); 



        if (cmd->bkg == false) 
        {
            /* -pgrp means wait for any process in pgrp */
            pid = waitpid(-pgrp, &status, WUNTRACED); 

            /* free process */
            node = list_find(pro_list, process_cmp, pid);
            remove_node(&pro_list, node, free_process);


            if(num_cmds > 1) 
            {
                pid = waitpid(-pgrp, &status, WUNTRACED);

                /* free process */
                node = list_find(pro_list, process_cmp, pid);
                remove_node(&pro_list, node, free_process);

            }
            /* free job */
            node = list_find(job_list, job_cmp, pgrp);
            remove_node(&job_list, node, free_job);

        }


        /* background process, execution continues in parent */
        else 
        { 
            printf("Running: %s\n", copy);
        }


        /* give terminal control back to shell */
        if( tcsetpgrp (STDIN_FILENO, getpgid(0)) == -1 )
        {
            perror(__func__);
            exit(EXIT_FAILURE);
        }
        

        free_cmd_arr(cmds_arr, num_cmds);
        free(copy);

    }
    return 0;
}
