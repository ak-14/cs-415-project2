#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "tokenizer.h"
#include "command.h"
#include "utils.h"
#include "list.h"
#include "process.h"
#include "job.h"


int my_strcmp(char *s1, char *s2) 
{
    int i;
    for(i = 0; s1[i] == s2[i]; i++) 
    {
        if(s1[i] == '\0' || s2[i] == '\0')
            break;
    }
    if(s1[i] == '\0' && s2[i] == '\0')
        return 1;
    return 0;
}


int my_strlen(char *str) 
{
    int i;
    for(i = 0; str[i] != '\0'; i++);
    return i;
}


char * my_strncpy(char *dest, const char *src, int size)
{              
    int i;
    for (i = 0; i < size && src[i] != '\0'; i++)
        dest[i] = src[i];
    for ( ; i < size; i++)
        dest[i] = '\0';
    return dest;
}


enum shell_cmd_t shell_cmds( char * buffer ) 
{
    TOKENIZER *tokenizer;
    char *tok;
    enum shell_cmd_t type;

    tokenizer = init_tokenizer( buffer );
    tok = get_next_token( tokenizer );

    if ( my_strcmp(tok, "exit") )
        type = EXIT;
    else if ( my_strcmp(tok, "fg") )
        type = FG;
    else if ( my_strcmp(tok, "bg") )
        type = BG;
    else if ( my_strcmp(tok, "jobs") )
        type = JOBS;
    else
        type = COMMAND;

    free(tok);
    free_tokenizer(tokenizer);
    return type;
}


int parse(char *buffer, command ***cmds_arr)
{
    TOKENIZER *tokenizer;
    char *tok, *pos;

    command **local_cmds_arr;
    command *cmd;
    char **args;

    int num_pipes, num_tokens, num_cmds;
    int args_i, cmds_i;
    int red_out, red_in, bkg;
    enum pipe_t pipe = NONE;
    char *file_in, *file_out, *file_err;

    int error = false;

    num_pipes = num_tokens = num_cmds = args_i = cmds_i = 0;

    tokenizer = init_tokenizer( buffer );

    /* save current position */
    pos = tokenizer->pos;

    /* count arguments and pipes */
    for (num_tokens = 1; (tok = get_next_token(tokenizer)) != NULL; num_tokens++) 
    {
        if (tok[0] == '|') 
            num_pipes++;
        free(tok);
    }

    /* reset position to beginning */
    tokenizer->pos = pos;

    /* allocate array of commands */
    num_cmds = num_pipes + 1;
    local_cmds_arr = calloc(num_cmds, sizeof(command));

    /* loop over commands seperated by pipes */
    while ((tok = get_next_token(tokenizer)) != NULL) 
    {
        /* reset files check for other variables too */
        file_in = NULL;
        file_out = NULL;
        file_err = NULL;
        pipe = NONE;
        bkg = 0;
        red_in = red_out = false;

        /* calloc arguments (add one for NULL if single command) */
        args = calloc(num_tokens, sizeof(char *));

        /* program name */
        args_i = 0;
        args[args_i++] =  tok;

        /* get the rest of the arguments */
        while ((tok = get_next_token(tokenizer)) != NULL && 
                tok[0] != '<' && tok[0] != '>' && !( tok[0] == '2' && tok[1] == '>') && 
                tok[0] != '|' && tok[0] != '&' )
        {
            args[args_i++] = tok;
        }

        if (tok == NULL && (num_pipes > 0))
        {
            pipe = IN;
        }

        while (tok != NULL) {

            switch (tok[0]) {
                case '|':
                    free(tok);

                    /* use commands array index to know which pipe */
                    if (cmds_i == 0) 
                    {
                        pipe = OUT;
                    }
                    else if (cmds_i < num_cmds)
                    {
                        pipe = BOTH;
                    } 

                    if (red_out) error = true;

                    break;

                case '&':
                    free(tok);

                    /* should be the last */
                    if ((tok = get_next_token(tokenizer)) != NULL)
                    {
                        free(tok);
                        error = true;
                    } else {
                        bkg = 1;
                    }
                    break;

                case '<':
                    free(tok);
                    if (pipe == IN)
                    {
                        error = true;
                        break;
                    }
                    red_in = 1;
                    file_in = tok = get_next_token(tokenizer);
                    continue;

                case '>':
                    free(tok);
                    red_out = 1;
                    file_out = tok = get_next_token(tokenizer);
                    continue;

                case '2':

                    if (tok[1] == '>') 
                    {
                        free(tok);
                        file_err = tok = get_next_token(tokenizer);
                        continue;
                    }

                default:
                    /* check if right side of pipe */
                    tok = get_next_token(tokenizer);

                    if (tok == NULL && num_pipes > 0)
                    {
                        pipe = IN;

                        if (red_in) {
                            error = true;
                            break;
                        } 
                    }
                    continue;
            }
            /* double break from switch and loop */
            break;
        }


        if ( error )
        {
            if (red_in)
                free(file_in);

            if (red_out)
                free(file_out);

            for (args_i = 0; args_i < num_tokens; args_i++) 
                free(args[args_i]);

            free(args);
            free_tokenizer(tokenizer);
            free_cmd_arr(local_cmds_arr, num_cmds);

            return -1;
        }

        cmd = build_command(args, num_tokens, bkg, file_in, file_out, file_err, pipe);

        local_cmds_arr[cmds_i++] = cmd;
    }            
    *cmds_arr = local_cmds_arr;
    free_tokenizer(tokenizer);

    return num_cmds;
}


void exec_cmd (command * cmd, int fds[][2]) {
    int red_in = 0, red_out = 0, red_err = 0;

    if (cmd->file_in != NULL) {
        if ( (red_in = open(cmd->file_in, O_RDONLY)) == -1) {
            perror(__func__);
            exit(EXIT_FAILURE);
        }
        dup2(red_in, STDIN_FILENO);
        close(red_in);
    }
    if (cmd->file_out != NULL) {
        if ( (red_out = open(cmd->file_out, O_WRONLY | O_CREAT, 0644)) == -1 ) {
            perror(__func__);
            exit(EXIT_FAILURE);
        }
        dup2(red_out, STDOUT_FILENO);
        close(red_out);
    }
    if (cmd->file_err != NULL) {
        if ( (red_err = open(cmd->file_err, O_WRONLY | O_CREAT, 0644)) == -1 ) {
            perror(__func__);
            exit(EXIT_FAILURE);
        }
        dup2(red_err, STDERR_FILENO);
        close(red_err);
    }

    if (cmd->pipe == OUT) {
        dup2(fds[0][1], STDOUT_FILENO);
        close(fds[0][0]);
    }

    if (cmd->pipe == IN) {
        dup2(fds[0][0], STDIN_FILENO);
        close(fds[0][1]);
    }

    if (cmd->pipe == BOTH) {
        dup2(fds[0][0], STDIN_FILENO);
        dup2(fds[0][1], STDOUT_FILENO);
    }

    if (cmd->pipe == NONE) {
        close(fds[0][0]);
        close(fds[0][1]);
    }


    execvp(cmd->args[0], cmd->args);
    perror(__func__);

}


int foregound (pid_t shell_pgrp, pid_t pgrp, int * status)
{   
    /* give terminal to pgrp */
    if ( tcsetpgrp (STDIN_FILENO, pgrp) == -1 )
    {
        perror(__func__);
        exit(EXIT_FAILURE);
    } 

    /* send SIGCONT to pgrp */
    if ( killpg(pgrp, SIGCONT) == -1 )
    {
        perror(__func__);
        exit(EXIT_FAILURE);
    } 

    /* wait for (-pgrp) which waits for a processes in pgrp */

    while ( waitpid(-pgrp, status, WUNTRACED) >= 0 )
    {
        //maybe do something here but probably out of the funciton
        //need to save pid or pgrp because wait will return -1 eventually
    }

    /* give terminal control back to shell */
    if ( tcsetpgrp (STDIN_FILENO, shell_pgrp) == -1 )
    {
        perror(__func__);
        exit(EXIT_FAILURE);
    } 
    return 0;
}

void report_jobs (node_t *pro_list, node_t *job_list ) 
{
    node_t *node;
    process_t *pro;
    job_t *job;
    int status;
    pid_t pid, pid2, pgrp;

    pid = waitpid(-1, &status, WNOHANG);

    while ( pid > 0) 
    {
        node = list_find(pro_list, process_cmp, pid);
        pro = (process_t *)node->data;

        pgrp = pro->pgrp;

        pid2 = waitpid(-pgrp, &status, WNOHANG);

        if (pid2 > 0)
        {
            pid = pid2;
            remove_node(&pro_list, node, free_process);
        }
        if (pid2 == 0)
        {

            remove_node(&pro_list, node, free_process);

        }
        if (pid2 < 0)
        {           

            remove_node(&pro_list, node, free_process);

            node = list_find(job_list, job_cmp, pgrp);
            job = (job_t *)node->data; 

            printf("Finished %s\n", job->job_name);

            remove_node(&job_list, node, free_job);
        }
        pid = waitpid(-1, &status, WNOHANG);

    }
}

/* unfinished */
void jobs (node_t *job_list) {

    node_t *curr = job_list;
    job_t *job;
    int i = 1;

    while (curr != NULL) {
        job = curr->data;
        printf("[%d] %s ", i, job->job_name);

        i++;
        curr = curr->next;
    }
}
