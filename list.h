#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct node_s {
    void *data;
    struct node_s *next;
    struct node_s *prev;
    int size;
} node_t;


void remove_node (node_t **list, node_t *node, void(*func)(void *));


node_t *push(node_t *list, void *data);


node_t *list_find(node_t *node, int(*func)(void*,pid_t), pid_t id);


void destroy_list(node_t **list, void(*func)(void*));

